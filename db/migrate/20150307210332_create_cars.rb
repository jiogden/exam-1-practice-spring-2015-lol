class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :owner_id
      t.string :make
      t.string :model
      t.string :vin_number

      t.timestamps null: false
    end
  end
end
